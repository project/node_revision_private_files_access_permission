Node Revision Private Files Access Permission

Node Revision Private Files Access Permission

This module allows you to manage permissions for displaying private files by role of unpublished version of node. With this module, you can restrict access to any individual user role to view the private files of unpublished version of node. Once enabled, You can assign permissions for any role.

INSTALLATION:

Put the module in your drupal modules directory and enable it in admin/modules.

IMPORTANT!

Once you check the enable box and submit the page, unpublished version of private files will be accessible to admin user. You just set permissions on the permission settings page (/admin/people/permissions) to enable access to other user role.
